Open Vision 10.2 (New) enigma2 images [![Build Status](https://travis-ci.org/OpenVisionE2/openvision-development-platform.svg?branch=develop)](https://travis-ci.org/OpenVisionE2/openvision-development-platform) [![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
=====================================
![alt tag](https://raw.github.com/OpenVisionE2/openvision-development-platform/develop/meta-openvision/recipes-openvision/bootlogo/openvision-bootlogo/bootlogo.jpg)

# Download
* Bazı test resimlerini https://openvision.tech/stb-images/ adresinden indirebilirsiniz.

# Support
* Bunlar, mevcut OpenVision Ana Dalının GELİŞTİRME EN İYİ RESİMLERİ'dir ve yalnızca hata ayıklama amaçlıdır. Lütfen üretimde kullanmayınız.

* Lütfen https://forum.openvision.tech adresindeki forumumuzu veya https://github.com/OpenVisionE2/openvision-development-platform/issues adresindeki sorun sayfamızı kullanın

# Donate
* Open Vision'na Bağış Yapın
* https://forum.openvision.tech/app.php/donate

# Distribution

    Geliştirme sürümlerini derlemenize izin var mı? Evet!
    Geliştirme sürümlerini dağıtmanıza izin var mı? Evet, ancak yalnızca "resmi olmayan sürüm" etiketiyle.
    Görüntülerimizi klonlarla uyumlu hale getirmenize izin var mı? Evet!
    Klonlanmış görüntüleri dağıtmanıza izin var mı? Evet, ancak yalnızca "klon sürümü" etiketiyle.
    Klonları destekliyor muyuz? Hayır!
    Resimlerimizin yedeğini almanıza izin var mı? Evet!
    Yedek görüntüleri dağıtmanıza izin var mı? Evet, ancak yalnızca "yedek sürüm" etiketiyle.
    Kendi kendine bina için destek sağlıyor muyuz? Hayır! Projelerimiz üzerinde çalışmayı tercih ediyoruz.
    Daha fazla Vision Developers'a ihtiyacımız var mı? Kesinlikle evet! Gitimize katkıda bulunun, sizinle iletişime geçelim!
    Daha fazla Görme Test Cihazına ihtiyacımız var mı? Kesinlikle evet! Görsellerimizi test edin ve hataları bildirin, büyümemize yardımcı olun, başlık akışlarını göreceksiniz.
    İmajımızı çoklu önyükleme durumlarında destekliyor muyuz? Hayır! Ekranı kontrol edin ve modülümüz yüklü değilse kendi başınızasınız!

Modelinizi https://github.com/OpenVisionE2/openvision-development-platform/blob/develop/Vision-metas.md de görürseniz, bunun için Open Vision olacağı anlamına gelir, aksi takdirde https://forum.openvision.tech/viewtopic.php?f=2&t=30 ve panoyu spam yapmayın!